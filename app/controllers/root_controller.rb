class RootController < ApplicationController
  def index
    render component: 'Main', props: { path: request.path }
  end
end
