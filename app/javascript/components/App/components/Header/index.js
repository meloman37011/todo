import React, { memo } from "react";
import { HeaderStyled } from "./style";

const Header = () => {
  return <HeaderStyled>header</HeaderStyled>;
};

export default memo(Header);
