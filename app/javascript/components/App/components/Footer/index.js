import React, { memo } from "react";
import { FooterStyled } from "./style";

const Footer = () => {
  return <FooterStyled>footer</FooterStyled>;
};

export default memo(Footer);
