import React, { memo } from "react";
import { ContentStyled } from "./style";

const Content = () => {
  return <ContentStyled>content</ContentStyled>;
};

export default memo(Content);
