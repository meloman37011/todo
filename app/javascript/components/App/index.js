import React, { memo } from "react";
import { AppStyled } from "./style";
import Header from "./components/Header";
import Content from "./components/Content";
import Footer from "./components/Footer";

const App = () => {
  return (
    <AppStyled>
      <Header />
      <Content />
      <Footer />
    </AppStyled>
  );
};

export default memo(App);
