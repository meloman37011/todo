import React, { memo } from "react";
import { BrowserRouter, StaticRouter } from "react-router-dom";
import App from "./App";

const Main = ({ path }) => {
  const Router = typeof window !== "undefined" ? BrowserRouter : StaticRouter;

  console.log(path);

  return (
    <Router location={path} context={{}}>
      <App />
    </Router>
  );
};

export default memo(Main);
