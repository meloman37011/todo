Rails.application.routes.draw do
  root 'root#index'

  match '*path', to: 'root#index', via: :all
end
